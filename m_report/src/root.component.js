import React from 'react';
import {Provider, connect} from 'react-redux';


export default class Root extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            Name : localStorage.getItem('Name')
        }
    }

    componentDidCatch(error, info) {
      console.log(error, info);
    }

    clear = () =>{
        localStorage.removeItem('Name')
        this.setState(x =>{
            return {Name : localStorage.getItem('Name')}
        })
    }
    render() {
        const {Name} = this.state
        return(
            <div>
                Report {Name}<br/>
                <button onClick={this.clear}>Clear Storage</button>
            </div>

        );
    }
}
