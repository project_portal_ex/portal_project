import React from "react";
import Chart from "./component/chart/chart.component"
import request from "./service/index"

export default class Root extends React.Component {
  state = {
    store: this.props.store,
    globalEventDistributor: this.props.globalEventDistributor,
    dataOpt : null,
    dataChart : null
  };
  async componentWillMount(){
    const optUrl = {url : 'http://localhost:5001/data'}
    const chartDataUrl = {url : 'http://localhost:5001/chartData'}
    this.setState({
      dataOpt : await request(optUrl).then(d => d ),
      dataChart : await request(chartDataUrl).then(d => d)
    })
  }

  componentDidCatch(error, info) {
    console.log(error, info);
  }

  render() {
    return(
      <div>
      { this.state.dataChart && this.state.dataOpt &&
          <Chart optionList = {this.state.dataOpt} chartList = {this.state.dataChart}/>
      }
      </div>
    )
  }
}