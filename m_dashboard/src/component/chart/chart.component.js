import {Select, DatePicker, Modal} from "antd";
const {RangePicker,MonthPicker} = DatePicker;
import React from "react";
import {Line} from 'react-chartjs-2';
let formatDate = require("./format");
const {Option} = Select;

export default class Chart extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            optionList : props.optionList,
            chartList : props.chartList,
            rangePicker : null,
            chartData : {},
            visible : false,
            selectOption : null,
            monthSelect : null
        }
    }

   
    selectItem = () => {
        const {optionList} = this.state;
        return (
            Object.values(optionList).map((item,id)=>{
                return <Option key={id+1} value={item.value} onClick={this.action}>{item.name}</Option>
            })
        );
    }
    
    updateChart = (type,range = {})=>{
        let {chartList} = this.state;
        let formatList = formatDate.format(chartList,type, range);
        this.setState({
            chartData : {
                labels : formatList.name,
                datasets : [
                    {
                        label : "Total : "+formatList.SumIncome,
                        data : formatList.val,
                        backgroundColor : 'transparent',
                        borderColor : 'magenta',
                        fill : "origin"
                    }
                ]
            }
        })
    }
    action = (e)=>{
        this.state.selectOption = e.key;
        if(e.key != "custom" && e.key != "period"){
            this.updateChart(e.key);
        }
        else{
            this.showModal()
        }
    }

    periodOrCustom = ()=>{
        const dateFormat = "YYYY/MM/DD";
        if (this.state.selectOption == "custom"){
            return (<RangePicker  format={dateFormat} style={{marginLeft : "50px"}} onChange={this.change}/>);
        }
        else if(this.state.selectOption == "period"){
            return (<MonthPicker onChange = {this.change} placeholder="Select month" style={{marginLeft : "150px"}}/>);
        }
    }

    componentWillMount(){
        this.updateChart("week");
    }
    change = (e)=>{
        if (this.state.selectOption == "custom"){
            this.setState({ rangePicker : {start : e[0]._d,end : e[1]._d} })
        }
        else{
            this.setState({rangePicker : {start : e._d,end : null }})
        }
    }
    showModal = () => this.setState({visible: true});
    handleOk = (e) => {
        this.setState({visible: false});
        this.updateChart(this.state.selectOption,this.state.rangePicker);
    }
    handleCancel = (e) => this.setState({visible: false});

    render(){
        
        return(
            <div>
                <Modal
                title="Select"
                visible={this.state.visible}
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                okButtonProps={{ disabled: false}}
                >
                    
                    {this.periodOrCustom()}
                </Modal>
                <Select defaultValue="Weekly" style={{width : '200px'}}>    
                    {this.selectItem()}            
                </Select>
                
                
                <Line
                    data={this.state.chartData}
                    width={100}
                    height={40}
                    options={{
                        title : {
                            display : true,
                            text : "Data Income",
                            fontSize : 30,
                        },
                        legend : {
                            
                            display : true,
                        },
                        layout : {
                            padding :{
                                left : 0,
                                right : 0,
                                bottom : 0,
                                top : 20
                            }
                        }
                    }}
                />
            </div>
        );
    }
}