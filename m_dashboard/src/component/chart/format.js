const moment = require('moment')

const FormatDate = {

    storage : {
        name : [],
        val : [],
        SumIncome : 0
    },
    dateString : '',
    getDateNames : (option,income, range = {}) => {
        let names = null
        const now = new Date(Date.now());
        let last,cek,end,start;

        let stringData =  FormatDate.dateString.toString().split(" ");
        if(option == "thisMonth"){
            last = moment().subtract(now.getDate() - 1,'day').format("YYYY/MM/DD");
            last = new Date(last);
            cek = FormatDate.dateString >= last && FormatDate.dateString <= now;
            cek ? names = stringData[2] : null;
        }
        else if(option == "week"){
            last = moment().subtract(now.getDay() - 1,'day').format("YYYY/MM/DD");
            last = new Date(last);
            cek = FormatDate.dateString >= last && FormatDate.dateString <= now;
            cek ? names = stringData[0] : null;
        }
        else if (option == "last30D"){
            last = moment().subtract(30,'day').format("YYYY/MM/DD");
            last = new Date(last);
            cek = FormatDate.dateString > last && FormatDate.dateString <= now;
            cek ? names =  stringData[1]+", "+ stringData[2] : null;
        }
        else if (option == "lastMonth"){
            last = moment().subtract(1,'month').format("YYYY/MM/DD");
            last = last.split("/"); last[2] = "01"; last = last.join("/");
            end = moment(last).endOf("month").format("YYYY/MM/DD");
            start = new Date(last);
            end = new Date(end);
            
            cek = FormatDate.dateString >= start && FormatDate.dateString <= end;
            cek ? names =  stringData[1]+", "+ stringData[2] : null;
        }
        else if (option == "custom"){
            cek = FormatDate.dateString >= range.start && FormatDate.dateString <= range.end;
            cek ? names =  stringData[1]+", "+ stringData[2] : null;
        }
        else if (option == "period"){
            start = range.start.toString().split(" ");
            start = start[3]+"/"+start[1]+"/"+"01";
            end = moment(start).endOf("month").format("YYYY/MM/DD");
            start = new Date(start);
            end = new Date(end);
            cek = FormatDate.dateString >= start && FormatDate.dateString <= end;
            cek ? names =  stringData[1]+", "+ stringData[2] : null;
        }
        
        // const index = FormatDate.storage.name.indexOf(names);
        if(names != null){
            FormatDate.storage.name.push(names);
            FormatDate.storage.val.push(income);
            FormatDate.storage.SumIncome += income;
        }
    },
    format : (data,option,range = {}) =>{ 
        FormatDate.storage.name = [];
        FormatDate.storage.val = [];
        FormatDate.storage.SumIncome = 0;
        Object.values(data).map((item,id)=>{    
            FormatDate.dateString = new Date(item.date); 
            FormatDate.getDateNames(option,item.incomeValue * 1,range);
        })
        return FormatDate.storage;
    }
}

module.exports = FormatDate;