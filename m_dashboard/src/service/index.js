import axios from 'axios'

export default async function request(option /* type = null */) {
  const { url, method = 'get', property = {}/* , page */ } = option
  const token = `JWT ${localStorage.getItem('token')}`
  const packages = {
    url,
    method,
    data: property,
    headers: { Authorization: token },
  }
  return (axios(packages)
    .then(response => response.data)
    .catch(er => er)
  )
}