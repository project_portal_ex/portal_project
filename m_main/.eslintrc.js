module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "extends" : "airbnb",
    "rules": {
        "semi" : [2, "never"],
        "no-console": "off",
        "indent": ["error", 2],
        "no-extra-semi": "error",
        "no-unexpected-multiline": "error"
    }
};