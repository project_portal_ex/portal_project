import 'zone.js';
import {start, registerApplication} from 'single-spa';
import { GlobalEventDistributor } from './globalEventDistributor'
import { loadApp } from './helper'

async function init() {
    const loadingPromises = [];

    registerApplication('bar', () => import('./main.app.js'), () => true);
    // declareChildApplication('User', () => import('../../portal_child/antd_master/src/route/user/index'), pathPrefix('/#/master/user') );

    // Port : 9001
    loadingPromises.push(loadApp('Dashboard', '/dashboard', '/dashboard/singleSpaEntry.js',null,null));

    // Port : 9002
    loadingPromises.push(loadApp('master', '/master', '/master/singleSpaEntry.js', null,null)); // does not have a store, so we pass null

    // Port : 9003
    loadingPromises.push(loadApp('report', '/report', '/report/singleSpaEntry.js', null, null));

    // wait until all stores are loaded and all apps are registered with singleSpa
    await Promise.all(loadingPromises);

    start();
}
init();
function pathPrefix(prefix) {
    return function(location) {
        return location.pathname.indexOf(`${prefix}`) === 0;
    }
}