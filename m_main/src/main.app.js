import React from 'react'
import ReactDOM from 'react-dom'
import singleSpaReact from 'single-spa-react'
import navbar from './component/navbar/navbar.component.js'

function domElementGetter() {
  return document.getElementById('bar')
}

const reactLifecyles = singleSpaReact({
  React,
  ReactDOM,
  rootComponent: navbar,
  domElementGetter,
})

export const bootstrap = [
  reactLifecyles.bootstrap,
]

export const mount = [
  reactLifecyles.mount,
]

export const unmount = [
  reactLifecyles.unmount,
]
