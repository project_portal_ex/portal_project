import React from 'react';
import ReactDOM from 'react-dom'
import { Button, Menu, Dropdown, Icon, Drawer, Layout, DatePicker, Breadcrumb } from 'antd'
import "antd/dist/antd.css";
import request from '../../service/index'
const { Header, Sider, Content } = Layout;
import style from './navbar.css'
const SubMenu = Menu.SubMenu;

function cekPath (obj){
  let keyId = null
  Object.values(obj).map((item,i)=>{
    if(window.location.hash.indexOf(item.name.toLowerCase()) != -1 ){
      keyId = (i+1).toString();
    }
  })
  return keyId
}

export default class Navbar extends React.Component {
  constructor(){
    super()
    this.state = {
      collapsed: false,
      ListMenu : null,
    };
  }

  componentDidCatch(error, info) {
    console.log(error, info);
  }

  async componentDidMount(){
    const opt = {url : 'http://localhost:5000/menu2'}
    this.setState({ListMenu : await request(opt).then(rs=>{
      return rs;
    })});
    
    this.componentBreadcumb(document.location.hash)
  }

  login = () =>{
    localStorage.setItem('Name','Aidil Febrian')
  }

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  }

  menuItem = (obj) =>{
    return (
      Object.values(obj).map((item)=>{
        if(!item.child){
          return(
            <Menu.Item  key={item.id}>
              <a href={item.url}>
                <Icon type={item.icon} />
                <span className="text">{item.name}</span>
              </a>
            </Menu.Item>

          );
        }
        else{
          return (
            <SubMenu key={item.id}
              title={<span><Icon type={item.icon} />
              <span className="text">{item.name}</span></span>}
            >
              {this.menuItem(item.child)}
            </SubMenu>
          );
        }
      })
    );
  }
  redirectBreadcumb = (p) =>{
    window.location.href = p.link
    this.componentBreadcumb(p.link)
  }
  componentBreadcumb = (path) => {
    path = !path ? [] : path.split('/')
    let pathStorage = []
    path.shift()

    if (path[0] != 'dashboard') pathStorage.push({link: '#/dashboard', name: 'dashboard' })
    path.map((p,i)=> {
      const indexPath = '#/'+path.slice(0,i+1).join('/');
      const item = {link: null, name: p }
      pathStorage.push(item)
    })
    
    const property = ( 
      <Breadcrumb>
        {
          pathStorage.map((p,i) => {
            if(p.link != null){
              return (
                <Breadcrumb.Item
                  key={i.toString()}
                  onClick = {() => this.redirectBreadcumb(p)}
                  href = {p.link}
                >
                  <span className = 'bcItem'>{p.name}</span>
                </Breadcrumb.Item>
              )
            }
            else {
              return (
                <Breadcrumb.Item key={i.toString()}>
                  <span className = 'bcItem'>{p.name}</span>
                </Breadcrumb.Item>
              )
            }
          })
        }
      </Breadcrumb>
    )
    
    ReactDOM.render(property,document.getElementById('bc'))
  }
  parsingBreadcumb = (e) => { 
    return this.componentBreadcumb(e.item.props.children.props.href)
  }
  menu = ()=>{
    if(this.state.ListMenu != null){
      return(
      <Menu theme="dark" mode="inline"
        defaultSelectedKeys={[cekPath(this.state.ListMenu)]}
        style={{marginTop : "50px"}}
        onClick = {this.parsingBreadcumb}
      >{this.menuItem(this.state.ListMenu)}</Menu>)
    }
  }
  buttonSize = {
    width : 50,
    height : 30,
    textAlign : 'center'
  }

  render() {
    const {ListMenu} = this.state
    return(
      <div style={{marginTop: 0}}>
        <Layout>
          <Sider
            trigger={null}
            collapsed={this.state.collapsed}
            width = {220}
            theme = "dark"
          >
          <div id="content_menu">
            {this.menu()}
          </div>
          </Sider>
          <Layout>
            <Header style={{ background: '#fff', padding: 0, height : 40 }}>
            <div style={{margin : '-12px 20px'}}>
              <Button type="default" style={this.buttonSize} onClick={this.toggle}>
                  <Icon
                  className="trigger"
                  type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                  />
              </Button>
              {/* <Button type="primary" style={this.buttonSize} onClick={this.login} >Login</Button> */}
            </div>          
            </Header>
            <div id="bc">
              
            </div>
            <Content style={{
              margin: '24px 16px', padding: 24, background: '#fff', minHeight: 690,
            }}
            >
              <div id="content"></div>
              <div id="master_content"></div>
            </Content>
          </Layout>
        </Layout>
      </div>
    );
  }
}

