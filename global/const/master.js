import request from '../../../../portal/src/service/index'

export default({
  namespace: 'master',
  state: {
    pagination : "asd",
    columns : null,
    data : "",
    Total : 0,
    Search : {},
    Flag : false,
    FlagSearch : false,
    panel : {
      disable : true,
      key : "1",
      text : {
        Title : "",
      }
    },
    PAGE : null,
    LIMIT : null
  },
  reducers: {
    searchMethod(state,{payload : {Search}}){
      return {...state, Search}
    },
    save(state,{payload : {number}}){
      return{...state, pagination : `?_page=${number}&_limit=5`}
    },
    UserSave(state, {payload : {Data, PAGE, Total, Search}}){
      return{...state,data : Data, PAGE, Total,Search,Flag : true}
    },
    columnsTable(state,{payload : {columns}}){
      return{...state, columns}
    },
    TabShow(state,{payload : {panel}}){
      return{...state,panel}
    }
  },
  effects: {
    *exec({payload : {number}},{call, put}){
      yield put({
        type : 'save',
        payload : {number}
      })
    },
    *fetch({payload : {PAGE = 1, Search = false}},{put,select}){
      const {SIZE_PAGE} = require('./limit-page');
      let cols = yield select(state => state.master.columns);

      let paginate;
      if(Search){
        paginate = "";
        cols = cols.map((i,x) => x != cols.length - 1 ? i.dataIndex : null);
      }
      else{
        paginate = `?_page=${PAGE}&_limit=${SIZE_PAGE}`;
      }

      const Url = {url : `http://localhost:5002/dataSource${paginate}`,
                    property : {value : Search, Col : cols}, page : {start : PAGE, end : SIZE_PAGE}}
      const Data = yield (request(Url));// <<<<
      const Total = yield (request({url :'http://localhost:5002/dataSource',
                            property : {value : Search, Col : cols}, page:{}}, "Count"))
      yield put({ type : 'UserSave',payload : {Data, PAGE, Total, Search }})
    },
    *search({payload : {Search}}, {put}){
      yield put({type : 'fetch', payload : {Search} })
    },
    *swap({payload : {panel}},{put}){
      yield put({type : 'TabShow', payload : {panel}})
    },
    *create({payload : {property}},{put}){
      property.id = yield (request({url : "http://localhost:5002/dataSource"},"Index"))
      const Url = {url : "http://localhost:5002/dataSource", method : "post", property }
      const throwback = yield (request(Url));
      yield put({type : 'reload',payload : {BACK : false}});
    },
    *delete({payload : {id}},{put}){
      const Url = {url : `http://localhost:5002/dataSource/${id}`, method : "delete"}
      const throwback = yield (request(Url))
      yield put({type : 'reload',payload : {BACK : false}});
    },
    *update({payload : {property}},{select,put}){
      const ID = yield select(state => state.master.modal.id)
      const Url = {url : `http://localhost:5002/dataSource/${ID}`, method : "patch", property }
      const throwback = yield (request(Url));
      yield put({type : 'reload', payload : {BACK : false}});
    },
    *col(action,{put}){
      const Url = {url : 'http://localhost:5002/columns'}
      const columns = yield (request(Url));
      yield put({ type : 'columnsTable',payload : {columns}})
    },
    *reload({payload : {BACK}},{put,select}){
      const PAGE = BACK ? 1 : yield select(state => state.master.PAGE);
      yield put({type : 'fetch',payload : {PAGE : PAGE}})
    }
  },
  subscriptions: {
    setup({ dispatch, history }) {
      return history.listen(({ pathname }) => {
        if (pathname === '/master/user') {
          dispatch({ type: 'fetch',payload : {PAGE : 1}});
          dispatch({ type: 'col'});
        }
      });
    },
  },

});