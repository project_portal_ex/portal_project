import React from "react";
import { connect } from 'dva';
import request from "../../service/index"
import TableComp from "../../component/table/table.component";
import FormComp from "../../component/form/form.component";
import ButtonComp from "../../component/button/button.component";
import PanelComp from "../../component/panel/tabPanel.component";
import Feedback from "../../component/feedback/feedback.component";

function EmployeApp ({master, dispatch}) {
  
  function moveTo(index) {
    const direction = master.direction
    dispatch({type : 'master/fetch', payload : {PAGE : index, Search : master.Search, direction}})
  }

  function swapForm (action ,data=null, id = null){
    const key = master.panel.key == "2" ? "1" : "2";
    const panel  = {disable : !master.panel.disable, key, env : {name : "master",action},data : data,
                    id}
    dispatch({type : 'master/swap', payload : {panel}})
  }

  async function addForm(){
    swapForm("create");
  }
  function updateForm(e){
    const index  = e.target.id;
    swapForm("update",master.data[index],master.data[index].employeeId);
  }

  function Search(e){
    const getData = master.Search ? master.Search.optional : "";
    const Search = {
      text : e,
      optional : getData
    }
    if(getData != "")
      dispatch({type : 'master/search', payload : {Search : Search}})
    else
      alert("Please Select Option ...")
  }
  function getFeedback(){
    let {success,message,show} = master.feedback
    if(show){
      Feedback(success,message);
      dispatch({type : 'master/showFeedback', payload : {show : false}})
    }
  }
  function deleteEmploye(e){
    const _id = master.data[e.target.id].employeeId;
    dispatch({type : 'master/delete', payload : {id : _id}})

  }
  function optionSelect(){
    return{
      mode : true,
      item : ["ID","Name","Address","Phone","Email","Position","All"],
      selected : (e) =>{
        const getData = master.Search ? master.Search.text : "";
        const Search = {
          text : getData,
          optional : e
        }
        dispatch({type : 'master/search', payload : {Search : Search}})
      }
    }
  }

  function reload(){
    dispatch({type : 'master/reload', payload : {BACK : true}});
  }
  
  async function getOptionApi(property){
    const save_property = property;
    if(property == "id type") property = "misc/code/IDTYPE?fields=miscName,miscDesc&for=user";
    else if (property == "city") property = "cities"
    else property = "employees/positions?fields=id,positionName&for=employee";

    const Url = {url : `http://192.168.80.64:6502/api/v1/${property}`}
    const result = await request(Url).then(rs =>{
      const data = []
      switch(save_property){
        case "id type":
          rs.data.map(it => { data.push({id : it.key, name : it.title, type : save_property})})
          break
        case "city":
          rs.data.map(it => { data.push({id : it.id, name : it.cityName, type : save_property})})
          break
        case "position":
          rs.data.map(it => { data.push({id : it.value, name : it.label, type : save_property})})
          break
      }
      return data;
    });
    return result
  }

  function mainContent (){
    return(
      <div style={{display : 'inline'}}>
        <div  style={{marginRight : '10px', float : 'left'}}>
          <ButtonComp type="add" actionClick = {addForm} />
        </div>
        <div style={{marginRight : '10px'}}>
          <ButtonComp type="reload" actionClick = {reload}/>
          <ButtonComp type="search" actionClick = {Search} option = {optionSelect()}/>
        </div>
        <TableComp
          Data = {master.data}
          Total = {master.Total}
          column = {master.columns}
          page = {master.PAGE}
          pageChange = {moveTo}
          delete = {deleteEmploye}
          update = {updateForm}
        />
      </div>
    );
  };


  function formContent(){
    return(
      <div>
        <FormComp
          dispatch = {dispatch}
          actionChange = {swapForm}
          typeForm = "divideCol"
          span = {12}
          id = {master.panel.id}
          lengthRow = {7}
          selectOption = {getOptionApi}
          feedback = {master.feedback}
          data = {master.panel.data}
          env = {master.panel.env}
          items = {master.inputForm}
        />
      </div>
    );
  }

  return(
    <div id="table">
    {getFeedback()}
    <PanelComp
      mainBody = {mainContent()}
      formBody = {formContent()}
      active = {master.panel.disable}
      keyPosition = {master.panel.key}
    />
    </div>
  );
}

function mapStateToProps(state) {
  return { master: state.master };
}
export default connect(mapStateToProps)(EmployeApp);