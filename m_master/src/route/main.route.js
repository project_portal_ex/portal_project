import dva from 'dva';
import { Router, Route, browserHistory, Switch } from 'dva/router';
import React from 'react';
import mod from '../model/master';
import EmployeeApp from './employee';
import SupplierApp from './supplier';
import CityApp from './city'

export default class MyData extends React.Component{

  componentWillMount(){
    const app = dva({history : browserHistory});
    app.model(mod);
    app.router(({history}) =>{
      return(
        <Router history={history}>
          <Switch>
            <Route path="/master/employee" component={EmployeeApp} />
            <Route path="/master/supplier" component={SupplierApp} />
            <Route path="/master/city" component={CityApp} />
          </Switch>
        </Router>
      );
    });
    app.start('#content');
  }

  componentDidCatch(er){
    console.log(er)
  }


  render(){
    return(
      <div></div>
    );
  }
}
