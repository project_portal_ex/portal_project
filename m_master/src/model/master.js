import request from '../service/index'

export default ({
  namespace: 'master',
  state: {
    pagination: 'asd',
    columns: null,
    inputForm: '',
    data: '',
    direction: '',
    Total: 0,
    Search: {},
    Flag: false,
    FlagSearch: false,
    panel: {
      disable: true,
      key: '1',
      env: {
        name: '',
        action: '',
      },
      data: null,
      id: '',
    },
    feedback: {
      success: false,
      show: false,
    },
    PAGE: null,
    LIMIT: null,
  },
  reducers: {
    searchMethod(state, { payload: { Search } }) {
      return { ...state, Search }
    },
    save(state, { payload: { number } }) {
      return { ...state, pagination: `?_page=${number}&_limit=5` }
    },
    MasterSave(state, {
      payload: {
        Data, PAGE, Total, Search, direction,
      },
    }) {
      return {
        ...state, data: Data, PAGE, Total, Search, Flag: true, direction,
      }
    },
    columnsTable(state, { payload: { columns, form, panel } }) {
      return {
        ...state, columns, inputForm: form, data: '', panel,
      }
    },
    TabShow(state, { payload: { panel } }) {
      return { ...state, panel }
    },
    hasFeedback(state, { payload: { feedback } }) {
      return { ...state, feedback }
    },
  },
  effects: {
    * exec({ payload: { number } }, { put }) {
      yield put({
        type: 'save',
        payload: { number },
      })
    },
    * fetch({ payload: { PAGE = 1, Search = false, direction } }, { put }) {
      const { SIZE_PAGE } = require('../../../global/const/limit-page')
      const Url = { url: `http://192.168.80.64:6502/api/v1/${direction}?page=${PAGE}&pageSize=${SIZE_PAGE}&m=bf` }
      const result = yield (request(Url))
      yield put({
        type: 'MasterSave',
        payload: {
          Data: result.data, PAGE, Total: result.total, Search, direction,
        },
      })
    },
    * search({ payload: { Search } }, { put }) {
      yield put({ type: 'fetch', payload: { Search } })
    },
    * swap({ payload: { panel } }, { put }) {
      yield put({ type: 'reload', payload: { BACK: true } })
      yield put({ type: 'TabShow', payload: { panel } })
    },
    * create({ payload: { values } }, { select, put }) {
      const direction = yield select(state => state.master.direction)
      const Url = { url: `http://192.168.80.64:6502/api/v1/${direction}`, method: 'post', property: values }
      const result = yield (request(Url))
      yield put({
        type: 'showFeedback',
        payload: {
          success: result.success,
          message: result.success ? 'Data has been created' : 'Data failed to created',
          show: true,
        },
      })
    },
    * delete({ payload: { id } }, { put, select }) {
      console.log(id)
      const direction = yield select(state => state.master.direction)
      const Url = { url: `http://192.168.80.64:6502/api/v1/${direction}/${id}`, method: 'delete' }
      const result = yield (request(Url))
      yield put({
        type: 'showFeedback',
        payload: {
          success: result.success,
          message: result.success ? 'Data has been deleted' : 'Data failed to deleted',
          show: true,
        },
      })
      yield put({ type: 'reload', payload: { BACK: true } })
    },
    * update({ payload: { values, id } }, { select, put }) {
      console.log(id)
      const direction = yield select(state => state.master.direction)
      const Url = { url: `http://192.168.80.64:6502/api/v1/${direction}/${id}`, method: 'put', property: values }
      const result = yield (request(Url))
      yield put({
        type: 'showFeedback',
        payload: {
          success: result.success,
          message: result.success ? 'Data has been update' : 'Data failed to update',
          show: true,
        },
      })
    },
    * showFeedback({ payload: { show, success = false, message = '' } }, { put }) {
      const feedback = {
        success,
        message,
        show,
      }
      yield put({ type: 'hasFeedback', payload: { feedback } })
    },
    * col({ payload: { direction } }, { put, select }) {
      const panel = yield select(state => state.master.panel)
      panel.key = '1'
      panel.disable = true
      const Url = { url: `http://localhost:5002/${direction}` }
      const result = yield (request(Url))
      yield put({ type: 'columnsTable', payload: { columns: result.columns, form: result.form, panel } })
    },
    * reload({ payload: { BACK } }, { put, select }) {
      const PAGE = BACK ? yield select(state => state.master.PAGE) : 1
      const direction = yield select(state => state.master.direction)
      yield put({ type: 'fetch', payload: { PAGE, direction } })
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      return history.listen(({ pathname }) => {
        if (pathname === '/master/employee') {
          dispatch({ type: 'fetch', payload: { PAGE: 1, direction: 'employees' } })
          dispatch({ type: 'col', payload: { direction: 'Employee' } })
        } else if (pathname === '/master/city') {
          dispatch({ type: 'fetch', payload: { PAGE: 1, direction: 'cities' } })
          dispatch({ type: 'col', payload: { direction: 'City' } })
        }
      })
    },
  },

})
