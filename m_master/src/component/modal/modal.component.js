import {Modal} from "antd"
import React from "react"

export default class ModalComp extends React.Component{
    constructor (props){
        super(props);
        this.state = {
            title : props.title,
            visible : props.visible,
            componentBody : props.Body || null,
            onOkFunc : props.okFunc,
            cancel : props.cancel
        }
    }
   
    render(){
        const {componentBody, onOkFunc, visible} = this.state;
        return(
            <div>
                <Modal
                title={this.state.title || "Default"}
                visible={visible}
                onOk={onOkFunc}
                onCancel={this.state.cancel}
                okButtonProps={{ disabled: false}}
                >
                    {componentBody}
                </Modal>
            </div>
            
        );
    }
}