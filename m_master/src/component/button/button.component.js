import {Icon, Button, Input, Select, Tooltip} from "antd";
import React, {Component} from "react";
import "antd/dist/antd.css";
import style from './button.less'

export default class ButtonComp extends Component{
  constructor(props){
    super(props);
    
  }
  optionItem = (item,selected = null) => {
    return (
      <Select defaultValue="Search By" style={{width : '120px'}} onSelect={selected}>
        {
          item.map((i,x)=>{
            return <Select.Option value={i} key={x}>{i}</Select.Option>
          })
        }  
      </Select>
    )
  }

  render(){
    const {type, actionClick, option, style} = this.props;
    switch(type){
      case "search":
        return( 
          <div style={{ display : 'inline',float : "right" }}>
          {option.mode ? this.optionItem(option.item, option.selected) : null}
          <Input.Search
            placeholder="input search text"
            onSearch={actionClick}
            style = {{width : "280px",left : '10px'}}
          /> 
          </div>
        )
      case "save":
        return (
          <Tooltip placement="bottom" title="Save">
            <Button className="btn btn-sv" type="primary" style={style} htmlType="submit"><Icon type="form" /></Button>
          </Tooltip>
        )
      case "add":
        return (<Tooltip placement="bottom" title="Add">
                <Button 
                className="btn btn-add"
                type="primary"
                onClick={actionClick}
                style={{marginBottom : "20px"}}
                >
                  <Icon type="plus" />
                </Button>
              </Tooltip>)
      case "reload":
          return ( 
            <Tooltip placement="bottom" title="Reload">
             <Button className="btn btn-df" type="primary" onClick={actionClick}><Icon type="reload" /></Button>
            </Tooltip>
          )
      case "goBack":
          return (
            <Tooltip placement="bottom" title="Back">
              <Button className="btn btn-df" type="primary" onClick={actionClick}><Icon type="arrow-left"/></Button>
            </Tooltip>
          )
          
    }
  }
}