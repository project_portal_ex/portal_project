import {
  message
} from 'antd';

export default function Feedback(flag,messages){
    switch(flag){
      case true:
        return message.success(messages);
      case false:
      return message.error(messages);
    }
}