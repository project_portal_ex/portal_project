import { Tabs, Radio } from 'antd';
import React from 'react';
const { TabPane } = Tabs;

export default class PanelComp extends React.Component{

  render() {
    const {size = "small",mainBody,formBody, active, keyPosition}  = this.props;
    return (
      <div>
        <Tabs tabBarExtraContent={<p>Aidil Febrian</p>} activeKey={keyPosition} size={size}>
          <TabPane tab="Employee" key="1" disabled={!active}>{mainBody}</TabPane>
          <TabPane tab="Form" key="2" disabled={active}>{formBody}</TabPane>
        </Tabs>
      </div>
    );
  }

}