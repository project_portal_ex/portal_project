import React from "react";
import { Table, Button, Menu, Dropdown, Icon, Pagination, Popover } from 'antd';
import {SIZE_PAGE} from "../../../../global/const/limit-page"
import style from "./table.css"


const {Column, ColumnGroup} = Table;
export default class TableComp extends React.Component {

  text = (
    <h4>Delete this user ?</h4>
  )
  content = (index) =>{
   return(
    <div>
      <Button id={index} type="danger" style={{margin : '0 15px'}} onClick={this.props.delete}> Yes </Button>
      <Button type="secondary" > No </Button>
    </div>
   );
  }
  menuAction = (index) => {
    return(
      <Menu >
        <Menu.Item>
          <Popover placement="topLeft" title={this.text} content={this.content(index)} trigger="click">
            <a rel="noopener noreferrer" >Delete</a>
          </Popover>
        </Menu.Item>
        <Menu.Item>
          <a  rel="noopener noreferrer" id={index} onClick={this.props.update}>Update</a>
        </Menu.Item>
      </Menu>
    );
  }

  dropDown = (index) =>{
    return(
        <Dropdown overlay={this.menuAction(index)}>
          <Button type="dashed">
             <Icon type="menu-unfold" /> <Icon type="down" />
          </Button>
        </Dropdown>
    )
  }

  remakeAction = ()=>{
    if(this.props.Data){
      this.props.Data.map((item,index) => {
        item.action = this.dropDown(index)
        item["key"] = item.employeeId || item.cityCode
      })
    }
  }

  sumWidth = (cols) =>{
    let width = 0;
    cols.map(it =>{
      width += it.width;
    })
    return width;
  }

  componentColumn = (item,key) =>{
    return(
    <Column
      title = {item.title}
      dataIndex = {item.dataIndex}
      key = {key}
      className = "tb"
      align = "center"
      width = {item.width}
      size = "small"
      fixed = {item.fixed || null}
    />
    )
  }
  setColumn = (cols) =>{
    return(
      cols.map((item,key)=>{
        if(item.child){
          return (
            <ColumnGroup className = "parentCol" title={item.title} width = {item.width} key = {key}>
              {
                item.child.map((it,i)=>{
                  return this.componentColumn(it,key + '.' + i)
                })
              }
            </ColumnGroup>
          )
        }
        else{
          return  this.componentColumn(item,key)
        }

      })
    )
  }
  render(){
    const {column, Data, pageChange, page, Total} = this.props;
    this.remakeAction();
    return(
      <div>

        { column && 
          <Table 
            size="small" 
            bordered
            scroll={{ x: this.sumWidth(column)}} 
            dataSource={Data || []}
            pagination={false}
          >
          {this.setColumn(column)}
          </Table>
        }
        {
          <Pagination
            total={Total}
            current={page}
            pageSize={SIZE_PAGE}
            style = {{float:'right', marginTop : '20px'}}
            onChange = {pageChange}
          />
        }
      </div>
    );


  }
}
