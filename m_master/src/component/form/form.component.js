import {
  Form, Input, Row, Col,Select, message
} from 'antd';
import style from './form.css';
import ButtonComp from '../button/button.component';
import React from 'react';


const Option = Select.Option;
const formItemLayout = {
  labelCol: {
    span: 8,         
  },
  wrapperCol: {
    span: 10, 
  }
}

class Forms extends React.Component {

  constructor(){
    super();
    this.state = {
      optionMenu : []
    }
  }

  getMessage = async () => {
    const {feedback} = this.props;
    if(feedback.message){
      if(this.props.env.action == "update") await this.props.actionChange ? this.props.actionChange (null) : null;
      this.props.form.resetFields();
    }
  }

  handleSubmit = (e) => {
    const {name,action} = this.props.env;
    const {id} = this.props;
    const type = name+"/"+action;
    this.props.form.validateFields((err, values) => {
      if (!err) {        
        this.props.dispatch({type : type, payload : {values,id}})
      }
    });
  }

  getOption = async (e) => {
    this.setState({
      optionMenu : await this.props.selectOption(e.toLowerCase())
    })
  }
  inputItem = (item) =>{
    if(["text","email"].indexOf(item.type) != -1)
    {
      return <Input  className="formItem"  type={item.type} autoComplete="off" />
    }
    else if(item.type == "option"){
      return (
        <Select className="formItem" onFocus ={() => this.getOption(item.fieldName)}>
          {
            this.state.optionMenu.map((opt,i)=>{
              if(item.fieldName.toLowerCase() == opt.type)
                return  <Option key={opt.id.toString()} value={opt.id} className="selectItem">{opt.name}</Option>
            })
          }
        </Select>
      )
    }
  }

  columnItem = (start,end) =>{
    const {items, data} = this.props;
    const { getFieldDecorator} = this.props.form;
    return (
      <div>
      {
          items.map((it,i) =>{
            if(i >= start && i < end ){
              return (
                <Form.Item 
                  hasFeedback = {it.require} 
                  className="formItem" 
                  {...formItemLayout}  
                  label={it.fieldName} key = {i}
                > 
                  {getFieldDecorator(it.index, {
                    initialValue : data != null ? data[it.index] : null,
                    
                    rules: [{ required: it.require}],
                  })(
                    this.inputItem(it)
                  )}
                </Form.Item>
              )
            }
        })
      }
      </div>
    )
  }

  divideCol = () => {
    const {items,span,lengthRow} = this.props;
    return (
      <Col span={24}>
        <Col span={span}> {this.columnItem(0, lengthRow * 1)} </Col>
        <Col span={span}>{items.length > lengthRow ? this.columnItem(lengthRow, 50) : null}</Col>
      </Col>
    )
  }

  formRender = () =>{
    const {typeForm} = this.props
    if(typeForm == "divideCol"){
      return this.divideCol();
    }
  }
  back = () => {
    this.props.actionChange(null);
    this.props.form.resetFields()
  }
  render() {
    this.getMessage();
    const {action} = this.props.env;
    const {id} = this.props;
    return (
      <Form onSubmit={this.handleSubmit}>
        <Form.Item style={{marginBottom : 0}}>
            {action == "update" ? <h2 className="idData">ID : {id}</h2> : null}
            <div style = {{float : 'right'}}>
              <span className="back">
                <ButtonComp type="goBack" actionClick={this.back}/>
              </span>
              <ButtonComp type="save"/>
            </div>
        </Form.Item>
        <div><hr width="100%" /><br/></div>
        <Row gutter = {8}>
          {this.formRender()}
        </Row>

      </Form>
    );
  }
}

export default Form.create({ name: 'FormComp' })(Forms);
