import React, {Component} from "react";
import TableComp from "../../../../portal/src/component/table/table.component";
import ModalComp from "../../../../portal/src/component/modal/modal.component";
import InputComp from "../../../../portal/src/component/input/input.component";
import ButtonComp from "../../../../portal/src/component/button/button.component";
import request from "../../../../portal/src/service/index";


export default class User extends Component{
  state = {
    columnTable : null,
    data : null,
    visibleFlag : false,
    storage : [],
    text : null,
    Body : null,
    cek : false,
    modeModal : null,
    updateData : {id: "",name : "",address : ""}
  }

  getData = (search = {}) =>{
    const UrlColumn = {url : 'http://localhost:5002/columns'}
    const UrlData = {url : 'http://localhost:5002/dataSource',property : search}
   
    request(UrlColumn,(rs)=>{ this.setState({columnTable : rs}) })
    request(UrlData,(rs)=>{ this.setState({data : rs,cek : true}) })
    
  }

  componentDidMount(){
    this.getData();
  }
  findIndex = (x) =>{
    let id = null
    this.state.data.map((it,i)=>{
      if (it.id == x) {id = i; }
    })
    return id
  }

  getIdSlot = () => {
    let slot = 0;
    this.state.data.map(item => {
      if(slot < item.id) slot = item.id*1;
    })
    return slot + 1;
  }

  add = () =>{
    const property = {
      id: this.getIdSlot().toString(),
      name: document.getElementById("item0").value,
      age: document.getElementById("item1").value.toString(),
      address: document.getElementById("item2").value,
      action: "X Y"
    }
    const Url = {url : "http://localhost:5002/dataSource", method : "post", property }
    request(Url,(rs)=>{
      this.getData()
    });
    
    this.setState({ cek : false })
  }

  update = ()=>{
    const {updateData : property} = this.state;
    const URL = {url : `http://localhost:5002/dataSource/${property.id}`, method : "patch",property }
    request(URL,(rs)=>{
      if(rs.flag){
        this.getData()
      }
      else{
        console.log(rs.Data);
      }
    })
  }

  delete = (e) => {
    
    const Url = {url : `http://localhost:5002/dataSource/${e.target.id}`,method : "delete"}
    request(Url);
    const n = this.findIndex(e.target.id);
    console.log(n);
    this.state.data.splice(n,1);
    this.setState({
      data : this.state.data
    })
  }

  typing = (e) =>{
    if(e.target.id == "data1")
      this.state.updateData.name = e.target.value;
    else 
      this.state.updateData.address = e.target.value;
  }

  updateForm = (e) =>{
    const Items = this.state.data.filter(item => item.id == e.target.id );
    this.state.updateData.id = e.target.id;
    this.state.updateData.name = Items.name;
    this.state.updateData.address = Items.address;
    this.setState({
      visibleFlag : true, storage : e.target.id, 
      modeModal : "update",
      text : "Update Data ...",
      Body : <InputComp datax = {Items} typing = {this.typing} type = {"update"} />,
    })
  }

  addForm = () =>{
    const Url = {url : "http://localhost:5002/formAddUser"}
    request(Url,(rs)=>{
      this.setState({
        visibleFlag : true,
        Body : <InputComp datax = {rs} type = {"add"} />,
        text : "Add Data ...",
        modeModal : "add"
      })
    })
  }

  handleCancel = (e) => this.setState({visibleFlag: false,cek : false});

  
  okFunc = () =>{
    if(this.state.modeModal == "update")
      this.update();
    else if (this.state.modeModal == "add"){
      this.add();
    }
    this.handleCancel();
  }

  modalComponents = () =>{
    return (
      this.state.visibleFlag && this.handleCancel && this.okFunc && 
      <ModalComp 
        title = {"User ..."}
        visible = {this.state.visibleFlag} 
        cancel = {this.handleCancel}
        okFunc = {this.okFunc}
        Body = {this.state.Body}
      />
    );
  }

  find = (x)=>{
    console.log(x);
    this.getData({value : x})
    this.setState({cek : false})
  }
  
  render(){
    const {columnTable,data} = this.state;
    return(
      <div id="table">
      <ButtonComp type="add" actionClick={this.addForm}/>
      <ButtonComp type="search" actionClick={ this.find }/>
      { this.modalComponents()}
      {
        (this.state && this.state.columnTable && this.state.data && this.delete && this.updateForm
          && !this.state.cek)
           &&   
        <div>
        <TableComp Data = {data} column = {columnTable} delete = {this.delete} update = {this.updateForm}/>  
        </div>
      }
      {
        (this.state && this.state.columnTable && this.state.data && this.delete && this.updateForm
          && this.state.cek)
           &&   
        <div>
        <TableComp Data = {data} column = {columnTable} delete = {this.delete} update = {this.updateForm}/>  
        </div>
      }
      </div>
    );
  }
}